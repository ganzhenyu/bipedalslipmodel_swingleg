%% (a) Display a stored solution
path(pathdef)
CurrentPath = pwd;
addpath(CurrentPath)
addpath([CurrentPath,filesep,'BipedalGaits;'])
addpath([CurrentPath,filesep,'Graphics;'])
addpath([CurrentPath,filesep,'StoredFunctions;'])
load('R2_v1_ALL.mat');
close all; clc;
k = 150;
xCYC = config(:,k);

% Show animation
[residual, T,Y,P,Y_EVENT,TE] = ZeroFunc_BipedApex(xCYC);
ShowTrajectory(T,Y,P,'Test')

[residual, T,Y,P,Y_EVENT,TE] = ZeroFunc_BipedApex(xCYC);
figure(2)
plot(T,Y,'LineWidth',2)
grid on; box on
xlim([0 T(end)]);
xlabel('Time $[\sqrt{l_o/g}]$','Interpreter','LaTex');

leg1 = legend('$x$','$\dot{x}$','$y$','$\dot{y}$'...
    ,'$\alpha_l$','$\dot{\alpha_l}$','$\alpha_r$','$\dot{\alpha_r}$');
set(leg1,'Interpreter','latex');
set(leg1,'FontSize',10);

%% (b) Display a whole branch
load('R2_v1_ALL.mat');
figure(3)
plot3(config(1,:),config(6,:),config(2,:),'LineWidth',2);
view([-45 45]); axis square;
box on; grid on;
xlabel('$\dot{x}  [\sqrt{gl_o}]$','Interpreter','LaTex')
ylabel('$\alpha_R$   $[rad]$','Interpreter','LaTex')
zlabel('$y  [l_o]$','Interpreter','LaTex')
%% (c) Continuation
% Solve for the first solution
xINIT = xCYC;
numOPTS = optimset('Algorithm','levenberg-marquardt',... 
                   'Display','iter',...
                   'MaxFunEvals',3000,...
                   'MaxIter',3000,...
                   'UseParallel', false,...
                   'TolFun',1e-12,...
                   'TolX',1e-12);
[xFINAL, ~] = fsolve(@ZeroFunc_BipedApex, xINIT, numOPTS);   

clear config
% Store the first periodic solution
config(:,1) = xFINAL;
% Add disturbance to the first solution
xINIT = xFINAL;
xINIT = xINIT + (rand-0.5)*1e-1;
% Find second solution
[xFINAL, ~] = fsolve(@ZeroFunc_BipedApex, xINIT, numOPTS);   
% Store the second solution
config(:,2) = xFINAL;


isp = 2;  
xCYC = config(:,isp); 
x_Last = xCYC;
stepSize = 0.01;

numOPTS = optimset('Algorithm','levenberg-marquardt',... 
                   'Display','iter',...
                   'MaxFunEvals',3000,...
                   'MaxIter',3000,...
                   'UseParallel', false,...
                   'TolFun',1e-12,...
                   'TolX',1e-12);
               
figure(4); clf; 
grid on; box on; hold on;
xlabel('$\dot{x}  [\sqrt{gl_o}]$','Interpreter','LaTex')
ylabel('$\alpha_R$   $[rad]$','Interpreter','LaTex')
zlabel('$y  [l_o]$','Interpreter','LaTex')
for i = isp + 1:1000
    clc
    disp(i);
    xINIT = xCYC + (config(:,i-1) - config(:,i-2));
    
    plot3(xINIT(1),xINIT(6),xINIT(2),'r.'); % Predictions
    drawnow();
    
    for TryTime = 1:2
    [xTry,Resi,exitflag,output,jacobian] = ContinuationFun(xINIT,x_Last,stepSize,numOPTS); 

    
    if Resi < 1e-9
        % This is a periodic solution
        break;
    end 
    
    % Otherwise, try a again   
    xINIT = xCYC + (config(:,i-1) - config(:,i-2))*((rand-0.5)*1e-2 + 1);
    end   
    
    if TryTime == 2
       disp('Can not find a solution within 2 trials.')
       break;
    else  
       xCYC = xTry;
    end
    
    x_Last = xCYC;
    config(:,i) = xCYC;    
    plot3(xCYC(1),xCYC(6),xCYC(2),'ko');
    drawnow();
    
    if xCYC(1)<-0.1
        break;
    end    
    
end    


